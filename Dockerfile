FROM ubuntu:22.04

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC \
    apt-get install -y \
        sudo time git-core subversion build-essential gcc-multilib \
        libncurses5-dev zlib1g-dev gawk flex gettext wget unzip \
        grep rsync python3 python3-distutils file && \
    apt-get clean

RUN useradd -m openwrt && \
    echo 'openwrt ALL=NOPASSWD: ALL' > /etc/sudoers.d/openwrt

USER openwrt
WORKDIR /home/openwrt
