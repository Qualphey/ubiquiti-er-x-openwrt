
# Build procedure
## Build docker container:
```
docker build -t er-x .
```
## Run the container with buildroot mounted within it:
```
docker run -it --mount src="$(pwd)",target=/openwrt,type=bind er-x
```
## Build the image
```
cd /openwrt
./scripts/feeds update -a
./scripts/feeds install -a
make menuconfig
make
```
